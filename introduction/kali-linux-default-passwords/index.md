---
title: Kali Linux Default Passwords
description:
icon:
type: archived
weight:
author: ["g0tmi1k",]
---

**This page is dated**. You can find the **latest version** here: [https://www.kali.org/docs/introduction/default-credentials/](/docs/introduction/default-credentials/).

- - -

During installation, Kali Linux allows users to configure a password for the _root_ user. However, should you decide to boot the live image instead, the i386, amd64, VMWare and ARM images are configured with the **default root password - "toor"**, without the quotes.
